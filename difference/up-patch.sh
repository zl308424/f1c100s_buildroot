#!/bin/bash

files='arch/arm/boot/dts/suniv-f1c100s.dtsi
arch/arm/boot/dts/suniv-f1c100s-licheepi-nano.dts
drivers/mtd/spi-nor/spi-nor.c
drivers/gpu/drm/sun4i/sun4i_tcon.c
drivers/gpu/drm/sun4i/sun4i_drv.c
drivers/gpu/drm/sun4i/sunxi_engine.h
drivers/gpu/drm/sun4i/sun4i_backend.h
drivers/gpu/drm/sun4i/sun4i_backend.c
drivers/phy/allwinner/phy-sun4i-usb.c
drivers/usb/musb/sunxi.c
drivers/video/fbdev/core/fbcon.c
'

rm -rf linux_kernel.patch

for f in $files; do
    diff -Nu a/$f b/$f >> linux_kernel.patch
done

