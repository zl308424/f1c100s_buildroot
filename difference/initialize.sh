#!/bin/sh

if [ ! -d a ]; then
    tar xvf ../dl-tar/linux/linux-5.2.11.tar.xz
    mv linux-5.2.11 a
fi

if [ ! -d b ]; then
    ln -s ../output/build/linux-5.2.11 b
fi

